build_rust:
	cargo build

build_wasm:
	cd webassembly && \
	wasm-pack build --target web && \
	cd .. && \
	cp webassembly/pkg/webassembly.js static/webassembly.js && \
	cp webassembly/pkg/webassembly_bg.wasm static/webassembly_bg.wasm && \
	rm -fr static/snippets && \
	cp -r webassembly/pkg/snippets static/snippets

run: build_wasm
	cargo run
