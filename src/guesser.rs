use rand::Rng;
use std::cmp::Ordering;
use std::env;

pub struct Guesser;

impl Guesser {
    pub fn generate_random_number() {
        env::set_var(
            "guess_number",
            rand::thread_rng().gen_range(0, 10).to_string(),
        )
    }

    fn number() -> i32 {
        env::var("guess_number").unwrap().parse().unwrap()
    }

    pub fn guess(number: i32) -> i32 {
        match Self::number().cmp(&number) {
            Ordering::Less => 1,
            Ordering::Equal => {
                Self::generate_random_number();
                0
            }
            Ordering::Greater => -1,
        }
    }
}
