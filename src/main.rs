#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate serde_json;
extern crate rand;

use actix_files as fs;
use actix_web::{web, App, HttpResponse, HttpServer};

pub mod guesser;
pub mod structs;
mod views;

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    guesser::Guesser::generate_random_number();

    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(views::index))
            .route("/guess", web::post().to(views::guess))
            .service(fs::Files::new("/static", "static").show_files_listing())
            .default_service(web::route().to(|| {
                HttpResponse::NotFound()
                    .content_type("text/plain; charset=utf-8")
                    .body("404")
            }))
    })
    .bind("0.0.0.0:8080")?
    .start()
    .await
}
