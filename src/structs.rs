#[derive(Serialize, Deserialize, Debug)]
pub struct GuessParams {
    pub number: i32,
}
