use crate::guesser::Guesser;
use crate::structs;
use actix_web::{web, HttpResponse, Responder};

pub async fn index() -> impl Responder {
    HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body(include_str!("../static/index.html"))
}

pub async fn guess(params: web::Json<structs::GuessParams>) -> impl Responder {
    let state = Guesser::guess(params.number);
    let state_json = json!({ "state": state });
    HttpResponse::Ok().json(state_json)
}
