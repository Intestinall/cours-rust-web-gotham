import init, * as wasm from "../static/webassembly.js";

async function run() {
    await init();
    wasm.main();
}
run();
