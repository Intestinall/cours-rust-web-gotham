import { __cargo_web_snippet_e9638d6405ab65f78daf4a5af9c9de14ecf1e2ec } from './snippets/stdweb-bb142200b065bd55/inline127.js';
import { __cargo_web_snippet_0aced9e2351ced72f1ff99645a129132b16c0d3c } from './snippets/stdweb-bb142200b065bd55/inline129.js';
import { __cargo_web_snippet_72fc447820458c720c68d0d8e078ede631edd723 } from './snippets/stdweb-bb142200b065bd55/inline130.js';
import { __cargo_web_snippet_97495987af1720d8a9a923fa4683a7b683e3acd6 } from './snippets/stdweb-bb142200b065bd55/inline131.js';
import { __cargo_web_snippet_dc2fd915bd92f9e9c6a3bd15174f1414eee3dbaf } from './snippets/stdweb-bb142200b065bd55/inline132.js';
import { __cargo_web_snippet_1c30acb32a1994a07c75e804ae9855b43f191d63 } from './snippets/stdweb-bb142200b065bd55/inline133.js';
import { __cargo_web_snippet_df4697739f13083d3632acec1e5cd1061696bf89 } from './snippets/stdweb-bb142200b065bd55/inline136.js';
import { __cargo_web_snippet_84339b1bf72a580059a6e0ff9499e53759aef5b9 } from './snippets/stdweb-bb142200b065bd55/inline15.js';
import { __cargo_web_snippet_ff5103e6cc179d13b4c7a785bdce2708fd559fc0 } from './snippets/stdweb-bb142200b065bd55/inline249.js';
import { wasm_bindgen_initialize } from './snippets/stdweb-bb142200b065bd55/inline340.js';
import { __cargo_web_snippet_2908dbb08792df5e699e324eec3e29fd6a57c2c9 } from './snippets/stdweb-bb142200b065bd55/inline378.js';
import { __cargo_web_snippet_3c5e83d16a83fc7147ec91e2506438012952f55a } from './snippets/stdweb-bb142200b065bd55/inline379.js';
import { __cargo_web_snippet_17f599d5adfb0d677b736d7fdbf842ea5974bd58 } from './snippets/stdweb-bb142200b065bd55/inline38.js';
import { __cargo_web_snippet_3f9a91c8d027e901dba4b1a3b6b7d3f129c977d1 } from './snippets/stdweb-bb142200b065bd55/inline401.js';
import { __cargo_web_snippet_614a3dd2adb7e9eac4a0ec6e59d37f87e0521c3b } from './snippets/stdweb-bb142200b065bd55/inline42.js';
import { __cargo_web_snippet_80d6d56760c65e49b7be8b6b01c1ea861b046bf0 } from './snippets/stdweb-bb142200b065bd55/inline427.js';
import { __cargo_web_snippet_9f22d4ca7bc938409787341b7db181f8dd41e6df } from './snippets/stdweb-bb142200b065bd55/inline429.js';
import { __cargo_web_snippet_b06dde4acf09433b5190a4b001259fe5d4abcbc2 } from './snippets/stdweb-bb142200b065bd55/inline43.js';
import { __cargo_web_snippet_a22b013cdfad43b048086eb38b7d14366fb7d483 } from './snippets/stdweb-bb142200b065bd55/inline47.js';
import { __cargo_web_snippet_0a02c7764aacc2ed939a149ae98000223d3eebe8 } from './snippets/stdweb-bb142200b065bd55/inline516.js';
import { __cargo_web_snippet_7c8dfab835dc8a552cd9d67f27d26624590e052c } from './snippets/stdweb-bb142200b065bd55/inline518.js';
import { __cargo_web_snippet_31f6071b77215fa0db4e2754d20833403f06a364 } from './snippets/stdweb-bb142200b065bd55/inline522.js';
import { __cargo_web_snippet_99c4eefdc8d4cc724135163b8c8665a1f3de99e4 } from './snippets/stdweb-bb142200b065bd55/inline527.js';
import { __cargo_web_snippet_ab05f53189dacccf2d365ad26daa407d4f7abea9 } from './snippets/stdweb-bb142200b065bd55/inline54.js';
import { __cargo_web_snippet_6fcce0aae651e2d748e085ff1f800f87625ff8c8 } from './snippets/stdweb-bb142200b065bd55/inline580.js';
import { __cargo_web_snippet_09c2e65cb945b3a07d756cb7c7369bfd2d291800 } from './snippets/stdweb-bb142200b065bd55/inline674.js';
import { __cargo_web_snippet_a25852c51af085f4ed732c8841ca8ff3a4c18fc1 } from './snippets/stdweb-bb142200b065bd55/inline676.js';
import { __cargo_web_snippet_8f3d27dc63d5d97a77a58ac099119b35169cccb4 } from './snippets/stdweb-bb142200b065bd55/inline677.js';
import { __cargo_web_snippet_a35b77319c5b64b2274ada5001b2f2b75a3d610a } from './snippets/stdweb-bb142200b065bd55/inline678.js';
import { __cargo_web_snippet_fc788667416ad6e036cdb7fca6aa20f52014d95d } from './snippets/stdweb-bb142200b065bd55/inline683.js';
import { __cargo_web_snippet_e4773373c7bd7977c8724df676307048e93c2570 } from './snippets/stdweb-bb142200b065bd55/inline686.js';
import { __cargo_web_snippet_9016940a8e29aebadf97e148b6d236edbc18b477 } from './snippets/stdweb-bb142200b065bd55/inline687.js';
import { __cargo_web_snippet_a152e8d0e8fac5476f30c1d19e4ab217dbcba73d } from './snippets/stdweb-bb142200b065bd55/inline81.js';
import { __cargo_web_snippet_f765b15a1a1b5cd266e922e6fca98dd570f17edc } from './snippets/stdweb-bb142200b065bd55/inline99.js';

let wasm;

const heap = new Array(32);

heap.fill(undefined);

heap.push(undefined, null, true, false);

function getObject(idx) { return heap[idx]; }

let heap_next = heap.length;

function dropObject(idx) {
    if (idx < 36) return;
    heap[idx] = heap_next;
    heap_next = idx;
}

function takeObject(idx) {
    const ret = getObject(idx);
    dropObject(idx);
    return ret;
}

function addHeapObject(obj) {
    if (heap_next === heap.length) heap.push(heap.length + 1);
    const idx = heap_next;
    heap_next = heap[idx];

    heap[idx] = obj;
    return idx;
}

let cachedTextDecoder = new TextDecoder('utf-8', { ignoreBOM: true, fatal: true });

cachedTextDecoder.decode();

let cachegetUint8Memory0 = null;
function getUint8Memory0() {
    if (cachegetUint8Memory0 === null || cachegetUint8Memory0.buffer !== wasm.memory.buffer) {
        cachegetUint8Memory0 = new Uint8Array(wasm.memory.buffer);
    }
    return cachegetUint8Memory0;
}

function getStringFromWasm0(ptr, len) {
    return cachedTextDecoder.decode(getUint8Memory0().subarray(ptr, ptr + len));
}
function __wbg_adapter_14(arg0, arg1, arg2, arg3) {
    wasm._dyn_core__ops__function__Fn__A__B___Output___R_as_wasm_bindgen__closure__WasmClosure___describe__invoke__hb295d61822021a14(arg0, arg1, arg2, arg3);
}

function __wbg_adapter_17(arg0, arg1, arg2) {
    var ret = wasm._dyn_core__ops__function__Fn__A____Output___R_as_wasm_bindgen__closure__WasmClosure___describe__invoke__h7d13649947d5567e(arg0, arg1, arg2);
    return ret;
}

/**
*/
export function main() {
    wasm.main();
}

let WASM_VECTOR_LEN = 0;

let cachedTextEncoder = new TextEncoder('utf-8');

const encodeString = (typeof cachedTextEncoder.encodeInto === 'function'
    ? function (arg, view) {
    return cachedTextEncoder.encodeInto(arg, view);
}
    : function (arg, view) {
    const buf = cachedTextEncoder.encode(arg);
    view.set(buf);
    return {
        read: arg.length,
        written: buf.length
    };
});

function passStringToWasm0(arg, malloc, realloc) {

    if (realloc === undefined) {
        const buf = cachedTextEncoder.encode(arg);
        const ptr = malloc(buf.length);
        getUint8Memory0().subarray(ptr, ptr + buf.length).set(buf);
        WASM_VECTOR_LEN = buf.length;
        return ptr;
    }

    let len = arg.length;
    let ptr = malloc(len);

    const mem = getUint8Memory0();

    let offset = 0;

    for (; offset < len; offset++) {
        const code = arg.charCodeAt(offset);
        if (code > 0x7F) break;
        mem[ptr + offset] = code;
    }

    if (offset !== len) {
        if (offset !== 0) {
            arg = arg.slice(offset);
        }
        ptr = realloc(ptr, len, len = offset + arg.length * 3);
        const view = getUint8Memory0().subarray(ptr + offset, ptr + len);
        const ret = encodeString(arg, view);

        offset += ret.written;
    }

    WASM_VECTOR_LEN = offset;
    return ptr;
}

let cachegetInt32Memory0 = null;
function getInt32Memory0() {
    if (cachegetInt32Memory0 === null || cachegetInt32Memory0.buffer !== wasm.memory.buffer) {
        cachegetInt32Memory0 = new Int32Array(wasm.memory.buffer);
    }
    return cachegetInt32Memory0;
}

function init(module) {
    if (typeof module === 'undefined') {
        module = import.meta.url.replace(/\.js$/, '_bg.wasm');
    }
    let result;
    const imports = {};
    imports.wbg = {};
    imports.wbg.__wbg_new_59cb74e423758ede = function() {
        var ret = new Error();
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_stack_558ba5917b466edd = function(arg0, arg1) {
        var ret = getObject(arg1).stack;
        var ptr0 = passStringToWasm0(ret, wasm.__wbindgen_malloc, wasm.__wbindgen_realloc);
        var len0 = WASM_VECTOR_LEN;
        getInt32Memory0()[arg0 / 4 + 1] = len0;
        getInt32Memory0()[arg0 / 4 + 0] = ptr0;
    };
    imports.wbg.__wbg_error_4bb6c2a97407129a = function(arg0, arg1) {
        try {
            console.error(getStringFromWasm0(arg0, arg1));
        } finally {
            wasm.__wbindgen_free(arg0, arg1);
        }
    };
    imports.wbg.__wbindgen_object_drop_ref = function(arg0) {
        takeObject(arg0);
    };
    imports.wbg.__wbindgen_object_clone_ref = function(arg0) {
        var ret = getObject(arg0);
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_cargowebsnippete9638d6405ab65f78daf4a5af9c9de14ecf1e2ec_ad1e81894f802539 = function(arg0, arg1) {
        __cargo_web_snippet_e9638d6405ab65f78daf4a5af9c9de14ecf1e2ec(takeObject(arg0), arg1);
    };
    imports.wbg.__wbg_cargowebsnippet80d6d56760c65e49b7be8b6b01c1ea861b046bf0_5a8953894b8affd6 = function(arg0, arg1) {
        __cargo_web_snippet_80d6d56760c65e49b7be8b6b01c1ea861b046bf0(takeObject(arg0), arg1);
    };
    imports.wbg.__wbg_cargowebsnippet9f22d4ca7bc938409787341b7db181f8dd41e6df_f184afed978d4a95 = function(arg0, arg1) {
        __cargo_web_snippet_9f22d4ca7bc938409787341b7db181f8dd41e6df(takeObject(arg0), arg1);
    };
    imports.wbg.__wbg_cargowebsnippetb06dde4acf09433b5190a4b001259fe5d4abcbc2_d346638ea92aac60 = function(arg0, arg1, arg2) {
        __cargo_web_snippet_b06dde4acf09433b5190a4b001259fe5d4abcbc2(takeObject(arg0), arg1, arg2);
    };
    imports.wbg.__wbg_cargowebsnippet614a3dd2adb7e9eac4a0ec6e59d37f87e0521c3b_61a037ef81d9af77 = function(arg0, arg1, arg2) {
        __cargo_web_snippet_614a3dd2adb7e9eac4a0ec6e59d37f87e0521c3b(takeObject(arg0), arg1, arg2);
    };
    imports.wbg.__wbg_cargowebsnippetab05f53189dacccf2d365ad26daa407d4f7abea9_641d6b20c73343b4 = function(arg0, arg1, arg2) {
        __cargo_web_snippet_ab05f53189dacccf2d365ad26daa407d4f7abea9(takeObject(arg0), arg1, arg2);
    };
    imports.wbg.__wbg_cargowebsnippeta22b013cdfad43b048086eb38b7d14366fb7d483_aa486e8d084840e5 = function(arg0, arg1, arg2) {
        __cargo_web_snippet_a22b013cdfad43b048086eb38b7d14366fb7d483(takeObject(arg0), arg1, arg2);
    };
    imports.wbg.__wbg_cargowebsnippet17f599d5adfb0d677b736d7fdbf842ea5974bd58_ce771c9250b89bd1 = function(arg0, arg1, arg2) {
        __cargo_web_snippet_17f599d5adfb0d677b736d7fdbf842ea5974bd58(takeObject(arg0), arg1, arg2);
    };
    imports.wbg.__wbg_cargowebsnippet0aced9e2351ced72f1ff99645a129132b16c0d3c_13e902b8d846fb01 = function(arg0, arg1) {
        var ret = __cargo_web_snippet_0aced9e2351ced72f1ff99645a129132b16c0d3c(takeObject(arg0), arg1);
        return ret;
    };
    imports.wbg.__wbg_cargowebsnippet31f6071b77215fa0db4e2754d20833403f06a364_7c18421e662884db = function(arg0, arg1) {
        var ret = __cargo_web_snippet_31f6071b77215fa0db4e2754d20833403f06a364(takeObject(arg0), arg1);
        return ret;
    };
    imports.wbg.__wbg_cargowebsnippet7c8dfab835dc8a552cd9d67f27d26624590e052c_0fd666abcb082554 = function(arg0, arg1) {
        var ret = __cargo_web_snippet_7c8dfab835dc8a552cd9d67f27d26624590e052c(takeObject(arg0), arg1);
        return ret;
    };
    imports.wbg.__wbg_cargowebsnippet84339b1bf72a580059a6e0ff9499e53759aef5b9_4e8ea0b89beeafa8 = function(arg0, arg1) {
        var ret = __cargo_web_snippet_84339b1bf72a580059a6e0ff9499e53759aef5b9(takeObject(arg0), arg1);
        return ret;
    };
    imports.wbg.__wbg_cargowebsnippet1c30acb32a1994a07c75e804ae9855b43f191d63_6d353463ef525961 = function(arg0) {
        __cargo_web_snippet_1c30acb32a1994a07c75e804ae9855b43f191d63(takeObject(arg0));
    };
    imports.wbg.__wbg_cargowebsnippetdc2fd915bd92f9e9c6a3bd15174f1414eee3dbaf_ce5c721cab10d020 = function(arg0) {
        __cargo_web_snippet_dc2fd915bd92f9e9c6a3bd15174f1414eee3dbaf(takeObject(arg0));
    };
    imports.wbg.__wbg_cargowebsnippet97495987af1720d8a9a923fa4683a7b683e3acd6_a438202dc16f44c0 = function(arg0, arg1, arg2) {
        __cargo_web_snippet_97495987af1720d8a9a923fa4683a7b683e3acd6(takeObject(arg0), arg1, arg2);
    };
    imports.wbg.__wbg_cargowebsnippet72fc447820458c720c68d0d8e078ede631edd723_ece3da0a4474dbeb = function(arg0, arg1, arg2, arg3) {
        __cargo_web_snippet_72fc447820458c720c68d0d8e078ede631edd723(takeObject(arg0), arg1, arg2, arg3);
    };
    imports.wbg.__wbg_cargowebsnippetf765b15a1a1b5cd266e922e6fca98dd570f17edc_9a90d0a0a5b7103b = function(arg0, arg1, arg2) {
        __cargo_web_snippet_f765b15a1a1b5cd266e922e6fca98dd570f17edc(takeObject(arg0), arg1, arg2);
    };
    imports.wbg.__wbg_cargowebsnippeta152e8d0e8fac5476f30c1d19e4ab217dbcba73d_d3aa4336afb90213 = function(arg0, arg1, arg2, arg3) {
        __cargo_web_snippet_a152e8d0e8fac5476f30c1d19e4ab217dbcba73d(takeObject(arg0), arg1, arg2, arg3);
    };
    imports.wbg.__wbg_cargowebsnippetdf4697739f13083d3632acec1e5cd1061696bf89_2a128fd825e3fb29 = function(arg0, arg1) {
        var ret = __cargo_web_snippet_df4697739f13083d3632acec1e5cd1061696bf89(takeObject(arg0), arg1);
        return ret;
    };
    imports.wbg.__wbg_cargowebsnippet3c5e83d16a83fc7147ec91e2506438012952f55a_23154670c635c22b = function(arg0, arg1) {
        var ret = __cargo_web_snippet_3c5e83d16a83fc7147ec91e2506438012952f55a(takeObject(arg0), arg1);
        return ret;
    };
    imports.wbg.__wbg_cargowebsnippetff5103e6cc179d13b4c7a785bdce2708fd559fc0_c2c7bf9cb65f32b6 = function(arg0, arg1) {
        __cargo_web_snippet_ff5103e6cc179d13b4c7a785bdce2708fd559fc0(takeObject(arg0), arg1);
    };
    imports.wbg.__wbg_wasmbindgeninitialize_c1c4df6b494511ad = function(arg0, arg1, arg2, arg3) {
        var ret = wasm_bindgen_initialize(takeObject(arg0), takeObject(arg1), getObject(arg2), getObject(arg3));
        return addHeapObject(ret);
    };
    imports.wbg.__wbindgen_cb_forget = function(arg0) {
        takeObject(arg0);
    };
    imports.wbg.__wbg_cargowebsnippet0a02c7764aacc2ed939a149ae98000223d3eebe8_0d0e2914f18ca5f5 = function(arg0, arg1) {
        var ret = __cargo_web_snippet_0a02c7764aacc2ed939a149ae98000223d3eebe8(takeObject(arg0), arg1);
        return ret;
    };
    imports.wbg.__wbg_cargowebsnippet3f9a91c8d027e901dba4b1a3b6b7d3f129c977d1_564da63d584c8e08 = function(arg0, arg1, arg2, arg3) {
        __cargo_web_snippet_3f9a91c8d027e901dba4b1a3b6b7d3f129c977d1(takeObject(arg0), arg1, arg2, arg3);
    };
    imports.wbg.__wbg_cargowebsnippet2908dbb08792df5e699e324eec3e29fd6a57c2c9_1aba12964286db2a = function(arg0, arg1) {
        var ret = __cargo_web_snippet_2908dbb08792df5e699e324eec3e29fd6a57c2c9(takeObject(arg0), arg1);
        return ret;
    };
    imports.wbg.__wbg_cargowebsnippet99c4eefdc8d4cc724135163b8c8665a1f3de99e4_14389f68322b60f1 = function(arg0, arg1, arg2, arg3, arg4) {
        __cargo_web_snippet_99c4eefdc8d4cc724135163b8c8665a1f3de99e4(takeObject(arg0), arg1, arg2, arg3, arg4);
    };
    imports.wbg.__wbg_cargowebsnippet6fcce0aae651e2d748e085ff1f800f87625ff8c8_21ca3c3552146790 = function(arg0, arg1) {
        __cargo_web_snippet_6fcce0aae651e2d748e085ff1f800f87625ff8c8(takeObject(arg0), arg1);
    };
    imports.wbg.__wbg_cargowebsnippete4773373c7bd7977c8724df676307048e93c2570_bd545fafd87f7efa = function(arg0, arg1) {
        __cargo_web_snippet_e4773373c7bd7977c8724df676307048e93c2570(takeObject(arg0), arg1);
    };
    imports.wbg.__wbg_cargowebsnippet9016940a8e29aebadf97e148b6d236edbc18b477_522d64919e52a279 = function(arg0, arg1) {
        var ret = __cargo_web_snippet_9016940a8e29aebadf97e148b6d236edbc18b477(takeObject(arg0), arg1);
        return ret;
    };
    imports.wbg.__wbg_cargowebsnippetfc788667416ad6e036cdb7fca6aa20f52014d95d_78cc0887ad2c63a2 = function(arg0, arg1, arg2, arg3) {
        __cargo_web_snippet_fc788667416ad6e036cdb7fca6aa20f52014d95d(takeObject(arg0), arg1, arg2, arg3);
    };
    imports.wbg.__wbg_cargowebsnippeta35b77319c5b64b2274ada5001b2f2b75a3d610a_65f741f32c188b97 = function(arg0, arg1, arg2) {
        __cargo_web_snippet_a35b77319c5b64b2274ada5001b2f2b75a3d610a(takeObject(arg0), arg1, arg2);
    };
    imports.wbg.__wbg_cargowebsnippet8f3d27dc63d5d97a77a58ac099119b35169cccb4_25e554ecfb96a8cd = function(arg0, arg1, arg2) {
        __cargo_web_snippet_8f3d27dc63d5d97a77a58ac099119b35169cccb4(takeObject(arg0), arg1, arg2);
    };
    imports.wbg.__wbg_cargowebsnippeta25852c51af085f4ed732c8841ca8ff3a4c18fc1_632e1b2f591dff6e = function(arg0, arg1, arg2, arg3) {
        __cargo_web_snippet_a25852c51af085f4ed732c8841ca8ff3a4c18fc1(takeObject(arg0), arg1, arg2, arg3);
    };
    imports.wbg.__wbg_cargowebsnippet09c2e65cb945b3a07d756cb7c7369bfd2d291800_9d3bc54b037ffae0 = function(arg0, arg1, arg2, arg3) {
        __cargo_web_snippet_09c2e65cb945b3a07d756cb7c7369bfd2d291800(takeObject(arg0), arg1, arg2, arg3);
    };
    imports.wbg.__wbindgen_throw = function(arg0, arg1) {
        throw new Error(getStringFromWasm0(arg0, arg1));
    };
    imports.wbg.__wbindgen_memory = function() {
        var ret = wasm.memory;
        return addHeapObject(ret);
    };
    imports.wbg.__wbindgen_function_table = function() {
        var ret = wasm.__wbindgen_export_0;
        return addHeapObject(ret);
    };
    imports.wbg.__wbindgen_closure_wrapper236 = function(arg0, arg1, arg2) {

        const state = { a: arg0, b: arg1, cnt: 1 };
        const real = (arg0, arg1) => {
            state.cnt++;
            try {
                return __wbg_adapter_14(state.a, state.b, arg0, arg1);
            } finally {
                if (--state.cnt === 0) {
                    wasm.__wbindgen_export_0.get(82)(state.a, state.b);
                    state.a = 0;
                }
            }
        }
        ;
        real.original = state;
        var ret = real;
        return addHeapObject(ret);
    };
    imports.wbg.__wbindgen_closure_wrapper234 = function(arg0, arg1, arg2) {

        const state = { a: arg0, b: arg1, cnt: 1 };
        const real = (arg0) => {
            state.cnt++;
            try {
                return __wbg_adapter_17(state.a, state.b, arg0);
            } finally {
                if (--state.cnt === 0) {
                    wasm.__wbindgen_export_0.get(82)(state.a, state.b);
                    state.a = 0;
                }
            }
        }
        ;
        real.original = state;
        var ret = real;
        return addHeapObject(ret);
    };

    if ((typeof URL === 'function' && module instanceof URL) || typeof module === 'string' || (typeof Request === 'function' && module instanceof Request)) {

        const response = fetch(module);
        if (typeof WebAssembly.instantiateStreaming === 'function') {
            result = WebAssembly.instantiateStreaming(response, imports)
            .catch(e => {
                return response
                .then(r => {
                    if (r.headers.get('Content-Type') != 'application/wasm') {
                        console.warn("`WebAssembly.instantiateStreaming` failed because your server does not serve wasm with `application/wasm` MIME type. Falling back to `WebAssembly.instantiate` which is slower. Original error:\n", e);
                        return r.arrayBuffer();
                    } else {
                        throw e;
                    }
                })
                .then(bytes => WebAssembly.instantiate(bytes, imports));
            });
        } else {
            result = response
            .then(r => r.arrayBuffer())
            .then(bytes => WebAssembly.instantiate(bytes, imports));
        }
    } else {

        result = WebAssembly.instantiate(module, imports)
        .then(result => {
            if (result instanceof WebAssembly.Instance) {
                return { instance: result, module };
            } else {
                return result;
            }
        });
    }
    return result.then(({instance, module}) => {
        wasm = instance.exports;
        init.__wbindgen_wasm_module = module;

        return wasm;
    });
}

export default init;

