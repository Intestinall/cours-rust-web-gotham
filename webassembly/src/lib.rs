extern crate stdweb;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;

mod utils;

use wasm_bindgen::prelude::*;
use stdweb::web::{document, IParentNode, IEventTarget, Element, IElement, XmlHttpRequest, XhrResponseType, XhrReadyState, INode};
use stdweb::web::event::{ClickEvent, ReadyStateChangeEvent};
use stdweb::web::html_element::InputElement;
use stdweb::unstable::TryInto;
use serde::Serialize;


// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[derive(Deserialize, Debug)]
struct TentacleGuessState {
    state: i32
}

struct FormPoster;

impl FormPoster {
    fn init_xhr(url: &str) -> XmlHttpRequest {
        let xhr = XmlHttpRequest::new();
        xhr.open("POST", url).unwrap();
        xhr.set_request_header("Content-Type", "application/json").unwrap();
        xhr.set_response_type(XhrResponseType::Text).unwrap();
        xhr
    }

    fn get_response(xhr: &XmlHttpRequest) -> Option<String> {
        match xhr.ready_state() {
            XhrReadyState::Done => {
                if xhr.status() == 200 {
                    Some(xhr.raw_response().into_string().unwrap())
                } else {
                    None
                }
            },
            _ => None
        }
    }

//    fn get_response<'a, T>(xhr: &XmlHttpRequest) -> Option<T>
//        where T: Deserialize<'a>
//    {
//        match xhr.ready_state() {
//            XhrReadyState::Done => {
//                if xhr.status() == 200 {
//                let text_response: String =  xhr.raw_response().into_string().unwrap();
//                let response: T = serde_json::from_str(&text_response).unwrap();
//                Some(response)
//            },
//            _ => None
//        }
//    }
}

#[derive(Serialize)]
struct Tentacle {
    number: u64
}

struct TentacleForm;

impl<'a> TentacleForm {
    const URL: &'a str = "/guess";

    fn get_button() -> Element {
        document().query_selector("#tentacles").unwrap().unwrap()
    }

    fn get_input_from(element: &impl IElement) -> InputElement {
        element.closest("form").unwrap().unwrap().query_selector("input").unwrap().unwrap().try_into().unwrap()
    }

    fn get_tentacle(button: &impl IElement) -> impl Serialize {
        let input: InputElement = Self::get_input_from(button);
        let tentacle_number = input.raw_value().parse::<u64>().unwrap();
        Tentacle{ number: tentacle_number }
    }

    fn get_tentacle_json(button: &impl IElement) -> String {
        serde_json::to_string(&Self::get_tentacle(button)).unwrap()
    }

    fn update_guess_number(state: TentacleGuessState) {
        let guess_state: Element = document().query_selector("#guess_state").unwrap().unwrap();
        let value_to_set = match state {
            TentacleGuessState { state: -1 } => "Greater !",
            TentacleGuessState { state: 0 } => "Congratulation ! You win !",
            TentacleGuessState { state: 1 } => "Lower !",
            _ => panic!("Incorrect state found : {}", state.state)
        };
        guess_state.set_text_content(value_to_set);
    }

    fn post(button: &impl IElement) {
        let tentacle_json = Self::get_tentacle_json(button);
        let xhr = FormPoster::init_xhr(Self::URL);

        xhr.add_event_listener({
            let xhr = xhr.clone();

            move |_: ReadyStateChangeEvent| {
//                match FormPoster::get_response::<TentacleGuessState>(&xhr) {

                if let Some(state) = FormPoster::get_response(&xhr) {
                    let state: TentacleGuessState = serde_json::from_str(&state).unwrap();
                    Self::update_guess_number(state);
                }
            }
        });
        xhr.send_with_string(&tentacle_json).unwrap();
    }
}

#[wasm_bindgen]
pub fn main() {
    utils::set_panic_hook();
    let button: Element = TentacleForm::get_button();

    button.add_event_listener( {
        let button = button.clone();
        move |_: ClickEvent| {
            TentacleForm::post(&button);
        }
    });
}
